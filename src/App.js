import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    let params = (window.location.search != "") ? params = window.location.search.match(/page=\d*/)[0].split('=')[1] : 1      
    params = (params <= 0) ? 1 : params;
        
    this.state = {
      peaple: props.cart,
      currentPage: Number(params),
      todosPerPage: 3
    };
    this.handleClick = this.handleClick.bind(this);
    this.nextPage = this.nextPage.bind(this);
    this.prevPage = this.prevPage.bind(this);
  } 

  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }
  nextPage() {
    let count = Math.ceil(this.state.peaple.length / this.state.todosPerPage)
    if (this.state.currentPage < count){
      this.setState({
        currentPage: this.state.currentPage + 1
      });
    }   
  }

  prevPage() {
    if (this.state.currentPage>1){
      this.setState({
        currentPage: this.state.currentPage - 1
      });
  }
  }

  render() {
    const { peaple, currentPage, todosPerPage } = this.state;

    // Logic for displaying current todos
    const indexOfLastTodo = currentPage * todosPerPage;
    const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
    const currentTodos = peaple.slice(indexOfFirstTodo, indexOfLastTodo);
  
    const renderTodos = currentTodos.map((peaple, index) => {
      return <li key={index}>{peaple.name}, age={peaple.age}</li>;
    });

    // Logic for displaying page numbers
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(peaple.length / todosPerPage); i++) {
      pageNumbers.push(i);
    }
    
    const renderPageNumbers = pageNumbers.map(number => {
      return (
        <li
          key={number}
          id={number}
          onClick={this.handleClick}
        >
          {number}
        </li>
      );
    });

    return (
      <div>
        <ul>
          {renderTodos}
        </ul>
        <ul id="page-numbers">
          <li onClick={this.prevPage} role="button for change list">Prev</li>
          {renderPageNumbers}
          <li onClick={this.nextPage} role="button for change list">Next</li>
        </ul>
      </div>
    );
  }
  
}

export default App;